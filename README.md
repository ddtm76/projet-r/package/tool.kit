
<!-- README.md is generated from README.Rmd. Please edit that file -->

# tool.kit

<!-- badges: start -->
<!-- badges: end -->

Le but du package est de fournir un certains nombres de fonctions utiles
pour faciliter la manipulation de fichier ou de dataframe.

## Installation

Vous pouvez installer le package depuis gitlab.

``` r
remotes::install_gitlab("ddtm76/projet-r/package/tool.kit")
```

## Example

``` r
library(tool.kit)
iris %>%
  clean_names() %>% 
  head()
#>   sepal_length sepal_width petal_length petal_width species
#> 1          5.1         3.5          1.4         0.2  setosa
#> 2          4.9         3.0          1.4         0.2  setosa
#> 3          4.7         3.2          1.3         0.2  setosa
#> 4          4.6         3.1          1.5         0.2  setosa
#> 5          5.0         3.6          1.4         0.2  setosa
#> 6          5.4         3.9          1.7         0.4  setosa
```

## Vignettes

Voir la vignette de [démarrage](docs/articles/demarrage.html)
