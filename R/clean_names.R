#' Nettoie les noms de colonne
#'
#' @param .data  dataframe ou vecteur
#' @param method_case c('snakecase', 'camelcase')
#' @param translit boolean retire les accents
#' @param unique boolean rend unique la colonne
#'
#' @return un dataframe ou un vecteur nettoye
#' @importFrom stringi stri_trans_general
#' @importFrom stringr str_c
#' @importFrom stopwords stopwords
#' @import ggplot2
#' @export
#'
#' @examples
#' clean_names(c("ma colonne a nettoyer", "colANettoyer"))
#' clean_names(c("ma colonne a nettoyer", "colANettoyer"), method_case = "snakecase")
clean_names <- function(.data, translit = TRUE, method_case = "snakecase", unique = FALSE) {
  n <- if (is.data.frame(.data)) colnames(.data) else .data
  stop_words <- stopwords::stopwords(language = "fr")

  n <- ggplot2:::snakeize(n) %>%
    gsub(perl = TRUE, "[[:punct:]]+", " ", .) %>% # la ponctuation
    gsub(word_match_list(stop_words), " ", .) %>% # retire les stopwords
    gsub("°", "numero", .) %>%
    gsub(perl = TRUE, "[[:space:]]+", "_", .) %>% # les espaces
    gsub(perl = TRUE, "^_+", "", .) %>% # les _ au debut
    gsub(perl = TRUE, "_+$", "", .) %>% # les _ a la fin
    gsub(perl = TRUE, "_+", "_", .)

  if (translit) {
    n <- stringi::stri_trans_general(n, "latin-ascii")
  }

  if (unique) {
    n <- make.unique(n, sep = "_")
  }

  n <- if (method_case == "camelcase") ggplot2:::camelize(n) else n

  if (is.data.frame(.data)) {
    colnames(.data) <- n
    .data
  } else {
    n
  }
}

# build word_regex from word list
word_match_list <- function(...) {
  words <- c(...)
  word_options <- paste(words, collapse = "|") # combine the words w/ | between them
  paste0("\\b(?:", word_options, ")\\b") # add extra regex formatting that makes it work
}
